terraform{
  required_providers{
    nutanix = {
      source = "nutanix/nutanix"
      version = "1.3.0"
    }
  }
}
provider "nutanix" {
  username  = "FNgankemAdmin@choiceophq.dev"
  password  = "June@2019"
  endpoint  = "10.101.0.51"
  insecure  = true
  port      = 9440
}
resource "nutanix_virtual_machine" "demo-01-web" {
  # General Information
  name                 = "C-DomainController"
  description          = "demo Frontend Web Server"
  num_vcpus_per_socket = 2
  num_sockets          = 1
  
  memory_size_mib      = 4096

  # What cluster will this VLAN live on?
  cluster_uuid = "0005b728-c69c-3c02-0000-00000000aa8b"

  # What networks will this be attached to?
  nic_list {
    # subnet_reference is saying, which VLAN/network do you want to attach here?
    subnet_uuid = "fc97edc1-0a3e-4722-9847-68358348b5ab"
    # Used to set static IP.
    # ip_endpoint_list {
    #   ip   = "10.xx.xx.xx"
    #   type = "ASSIGNED"
    # }
  }

  # What disk/cdrom configuration will this have?
  disk_list {
    # data_source_reference in the Nutanix API refers to where the source for
    # the disk device will come from. Could be a clone of a different VM or a
    # image like we're doing here
    data_source_reference = {
        kind = "image"
        uuid = "ce19745e-b149-49cc-a721-1df3a367226f"
      }


    device_properties {
      disk_address = {
        device_index = 0
        adapter_type = "SCSI"
      }

      device_type = "DISK"
    }
  }
  disk_list {
    # defining an additional entry in the disk_list array will create another.

    #disk_size_mib and disk_size_bytes must be set together.
    disk_size_mib   = 100000
    disk_size_bytes = 104857600000
  }
  provisioner "file" {
    connection {
      type     = "winrm"
      user     = "administrator"    # user from the image attached
      password = "$Choice123.123$" #password from the user
      host    = nutanix_virtual_machine.demo-01-web.nic_list_status[0].ip_endpoint_list[0].ip
}
    source      = "./EnableRemote.ps1"
    destination = "C:/TEMP/EnableRemote.ps1"
  }
  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      user     = "administrator"    # user from the image attached
      password = "$Choice123.123$" #password from the user
      host    = nutanix_virtual_machine.demo-01-web.nic_list_status[0].ip_endpoint_list[0].ip
} #self.nic_list_status[0].ip_endpoint_list[0].i


    inline = ["powershell C:/TEMP/EnableRemote.ps1"]
  }
}

# Show IP address
output "ip_address" {
  value = nutanix_virtual_machine.demo-01-web.nic_list_status[0].ip_endpoint_list[0].ip
}
