$interfaceIndex = (Get-NetAdapter | Where-Object {$_.InterfaceAlias -eq 'Ethernet'}).InterfaceIndex
$dnsServers = '10.101.0.115'  # Replace with your desired DNS server addresses
Set-DnsClientServerAddress -InterfaceIndex $interfaceIndex -ServerAddresses $dnsServers
