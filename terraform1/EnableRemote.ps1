$interfaceIndex = (Get-NetAdapter | Where-Object {$_.InterfaceAlias -eq 'Ethernet'}).InterfaceIndex
$dnsServers =   # Replace with your desired DNS server addresses
Set-DnsClientServerAddress -InterfaceIndex $interfaceIndex -ServerAddresses $dnsServers
