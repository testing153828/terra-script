terraform{
  required_providers{
    nutanix = {
      source = "nutanix/nutanix"
      version = "1.9.0"
    }
  }
}
provider "nutanix" {
  username  = "FNgankemAdmin@choiceophq.dev"
  password  = "June@2019"
  endpoint  = "10.101.0.51"
  insecure  = true
  port      = 9440
}
resource "nutanix_virtual_machine" "demo-01-web" {
  # General Information
  name                 = "A-DomainController"
  description          = "demo Frontend Web Server"
  num_vcpus_per_socket = 2
  num_sockets          = 1
  
  memory_size_mib      = 4096

  # What cluster will this VLAN live on?
  cluster_uuid = "0005b728-c69c-3c02-0000-00000000aa8b"

  # What networks will this be attached to?
  nic_list {
    # subnet_reference is saying, which VLAN/network do you want to attach here?
    subnet_uuid = "47840d1e-4582-4bc4-9380-c00ce6e8717f"
    # Used to set static IP.
    # ip_endpoint_list {
    #   ip   = "10.101.1.154"
    #   type = "ASSIGNED"
    # }

  }
  
  # What disk/cdrom configuration will this have?
  disk_list {
    # data_source_reference in the Nutanix API refers to where the source for
    # the disk device will come from. Could be a clone of a different VM or a
    # image like we're doing here
    data_source_reference = {
        kind = "image"
        uuid = "9e8f94c0-3d94-4633-9fb3-838b8fb068a8"
      }


    device_properties {
      disk_address = {
        device_index = 0
        adapter_type = "SCSI"
      }

      device_type = "DISK"
    }
  }
  disk_list {
    # defining an additional entry in the disk_list array will create another.

    #disk_size_mib and disk_size_bytes must be set together.
    disk_size_mib   = 100000
    disk_size_bytes = 104857600000
  }

  guest_customization_sysprep = {
    install_type = "PREPARED"
    unattend_xml = base64encode(file("sysprep1_autoattend.xml"))
 }
  provisioner "file" {
    connection {
      type     = "winrm"
      user     = "administrator"    # user from the image attached
      password = "MyPassword123" #password from the user
      host    = "10.101.1.203"
      #nutanix_virtual_machine.demo-01-web.nic_list_status[0].ip_endpoint_list[0].ip
}
    source      = "./EnableRemote1.ps1"
    destination = "C:/TEMP/EnableRemote1.ps1"
  }
  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      user     = "administrator"    # user from the image attached
      password = "MyPassword123" #password from the user
      host    = "10.101.1.203"
      #nutanix_virtual_machine.demo-01-web.nic_list_status[0].ip_endpoint_list[0].ip
} #self.nic_list_status[0].ip_endpoint_list[0].i


    inline = ["powershell C:/TEMP/EnableRemote1.ps1"]
  }
 }

# Show IP address
# output "ip_address" {
#   value = nutanix_virtual_machine.demo-01-web.nic_list_status[0].ip_endpoint_list[0].ip
# }
