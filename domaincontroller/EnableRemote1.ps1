# $DCAdminUsername = "Administrator"
# $DCAdminPassword = "MyPassword123"
# $DomainName = "Gitlab-6.choicesolutions.com"
# $DNSHostName = "Choice123"
# $SafeModePassword = "June@2018@2018@"

# # Create new service account
# $SecurePassword = ConvertTo-SecureString -String $DCAdminPassword -AsPlainText -Force
# $ServiceAccount = New-ADServiceAccount -Name "DCPromotionAccount" -SamAccountName "DCPromotionAccount" -DNSHostName $DNSHostName -Description "Service account for promoting the server to a Domain Controller" -PassThru -Credential (New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $DCAdminUsername, $SecurePassword)

# # Install Active Directory Domain Services feature
# Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools -IncludeAllSubFeature

# # Promote server to a Domain Controller
# $DCPromoParams = @{
#     DomainName = $DomainName
#     InstallDns = $true
#     NoRebootOnCompletion = $false
#     SafeModeAdministratorPassword = (ConvertTo-SecureString -String $SafeModePassword -AsPlainText -Force)
# }

# Install-ADDSForest @DCPromoParams -Confirm:$false

# # Set preferred DNS server
# $NIC = Get-NetAdapter | Where-Object {$_.Status -eq 'Up' -and $_.InterfaceAlias -ne 'Loopback'}
# $DNSServerParams = @{
#     InterfaceAlias = $NIC.InterfaceAlias
#     ServerAddresses = @($IPAddress)
#     Validate = $false
# }

# Set-DnsClientServerAddress @DNSServerParams

$DCAdminUsername = "Administrator"
$DCAdminPassword = "MyPassword123"
$DomainName = "Gitlab-6.choicesolutions.com"
$DNSHostName = "Choice123"
$SafeModePassword = "June@2018@2018@"
# $NewServerName = "OPLAGITLABDC"  # Add the desired server name here
# Rename-Computer -NewName $newServerName -Force

# Start-Sleep -Seconds 30


# Create new service account
$SecurePassword = ConvertTo-SecureString -String $DCAdminPassword -AsPlainText -Force
$ServiceAccount = New-ADServiceAccount -Name "DCPromotionAccount" -SamAccountName "DCPromotionAccount" -DNSHostName $DNSHostName -Description "Service account for promoting the server to a Domain Controller" -PassThru -Credential (New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $DCAdminUsername, $SecurePassword)

# Install Active Directory Domain Services feature
Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools -IncludeAllSubFeature

# Promote server to a Domain Controller
$DCPromoParams = @{
    DomainName = $DomainName
    InstallDns = $true
    NoRebootOnCompletion = $false
    SafeModeAdministratorPassword = (ConvertTo-SecureString -String $SafeModePassword -AsPlainText -Force)
}

Install-ADDSForest @DCPromoParams -Confirm:$false

# Set preferred DNS server
$NIC = Get-NetAdapter | Where-Object {$_.Status -eq 'Up' -and $_.InterfaceAlias -ne 'Loopback'}
$DNSServerParams = @{
    InterfaceAlias = $NIC.InterfaceAlias
    ServerAddresses = @($IPAddress)
    Validate = $false
}

Set-DnsClientServerAddress @DNSServerParams

