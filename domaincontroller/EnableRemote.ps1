# $interfaceIndex = (Get-NetAdapter | Where-Object {$_.InterfaceAlias -eq 'Ethernet'}).InterfaceIndex
# Set-NetIPInterface -InterfaceIndex $interfaceIndex -Dhcp Disabled
# Set-NetIPAddress -InterfaceIndex $interfaceIndex -IPAddress 10.101.1.201  -PrefixLength 24
# Set-DnsClientServerAddress -InterfaceIndex $interfaceIndex  -ServerAddresses 10.101.1.2,8.8.8.8
$interfaceIndex = (Get-NetAdapter | Where-Object {$_.InterfaceAlias -eq 'Ethernet'}).InterfaceIndex
#Get-NetIPInterface -AddressFamily IPv4
Set-NetIPInterface -InterfaceIndex $interfaceIndex -DHCP Disabled
New-NetIPAddress -InterfaceIndex $interfaceIndex -AddressFamily IPv4 -IPAddress "10.101.1.154" -PrefixLength 24 -DefaultGateway "10.101.1.1"
Set-DnsClientServerAddress -InterfaceIndex $interfaceIndex  -ServerAddresses "10.101.1.2"


Restart-Service WinRM
