$interfaceIndex = (Get-NetAdapter | Where-Object {$_.InterfaceAlias -eq 'Ethernet'}).InterfaceIndex
$dnsServers = '10.101.1.2'  # Replace with your desired DNS server addresses
Set-DnsClientServerAddress -InterfaceIndex $interfaceIndex -ServerAddresses $dnsServers
# adding the target sever to the Domain  
#$domainName = "Gitlab-5.choicesolutions.com"
#$username1 = "ansible"
#$password1 = ConvertTo-SecureString "Today@2018" -AsPlainText -Force
#$cred = New-Object System.Management.Automation.PSCredential ($username1, $password1)
#Add-Computer -DomainName $domainName -Credential $cred -OUPath "OU=Fileserver,DC=Gitlab-2, DC=choicesolutions,DC=com"
