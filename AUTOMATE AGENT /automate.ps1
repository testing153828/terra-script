$localFilePath = "C:\TEMP\Choice_Dev_Automate_Agent.MSI"

$apiUrl = "https://gitlab.choicesolutions.com/automate/automate_agent/-/raw/main/Choice_Dev_Automate_Agent.MSI?inline=false"

# Use Invoke-RestMethod to retrieve the raw contents of the script file

# Save the script contents to a local file

Invoke-WebRequest -Uri $apiUrl -OutFile $localFilePath


Start-Process -FilePath "msiexec.exe" -ArgumentList "/i `"$localFilePath`" /quiet" -restart
