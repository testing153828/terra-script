$interfaceIndex = (Get-NetAdapter | Where-Object {$_.InterfaceAlias -eq 'Ethernet'}).InterfaceIndex
Set-NetIPInterface -InterfaceIndex $interfaceIndex -Dhcp Disabled
Set-NetIPAddress -InterfaceIndex $interfaceIndex -IPAddress 10.101.1.201  -PrefixLength 24
Set-DnsClientServerAddress -InterfaceIndex $interfaceIndex  -ServerAddresses 10.101.1.2,10.101.1.1

